# gitlab

## presentation

- qu'est-ce que c'est 
- pourquoi lui et pas un concurent
  - [gitea](https://gitea.com/gitea/blog)
  - bitbucket
  - github
  - [pagure](https://pagure.io/Ambassade)
  - [sourcehut](https://git.sr.ht/~etalab/code.gouv.fr)

## git

- gestion des utilisateurs
- gestion du code
- merges
- revues de codes

## gestion de projet

- tickets
- kanban
- sprints
- youtrack
- avancement

## CI/CD

- ci
  - build artefacts
  - analyse statics (sonarcube, securité)
  - tests
  - lint
- cd
  - pages
  - pilotage/déploiement
  - releases

kubernetes, monitoring, etc, etc ...
